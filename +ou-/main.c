/* Welcome to the LessOrMore GAME 
 * made by : FreakyWaves
 * - the computer chooses a random number 
 * - then the user has to guess it 
 * - the number of tries is counted (the lesser the better)
 * - there are different levels of difficulty + manual set of minimum & maximum
 * - And AN H A R D C O R E MODE !!!
 *
 *  tried to solve the scanf infinite loop when a NaN is entered but I need help
 *
 * Enjoy :) 
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int MAX = 100, MIN = 1, HARD = 0;

int pscanf(char *scan, int ret,int OverLoadProtection)
{
    if(OverLoadProtection < 500)
    {
        int res = 0;
        res = scanf(scan, &ret);
        if (res != 1)
        {   /* Prevent the recursion from overloading the stack */
            OverLoadProtection++;
            /* Tell the user he has done something wrong */
            printf("\n[%d] Enter numbers ONLY", 500 - OverLoadProtection);
            pscanf(scan, ret, OverLoadProtection);
        }
        else if (res == 1)
        {
            return (ret);
        }
    }
    else 
    {
        printf("\n\n /!\\INPUT OVERLOAD EXCEPTION /!\\\n\n");
    }
    return (5);
}

int diff()
{
    int dif = 2;
    
    do
    {
        printf("\nSelect the range for the mystery number");
        printf("\n(0) enter the range manually");
        printf("\n(1) range between 1 and 10");
        printf("\n(2) range between 1 and 100");
        printf("\n(3) range between 1 and 500");
        printf("\n(4) range between 1 and 1000");
        printf("\n(5) range between 1 and 5000");
        printf("\nChoose difficulty : ");
        
        scanf(" %d", &dif);


    } while(dif < 0 || dif > 5); 

    MIN = 1;
    switch(dif)
    {
        case 0:
            return(1);
            break;
        case 1:
            printf("\nOkay you wanna go easy");
            MAX = 10;
            break;
        case 2:
            printf("\nSo you're an average person then");
            MAX = 100;
            break;
        case 3:
            printf("\nYou want some challenge, don't you ?");
            MAX = 500;
            break;
        case 4:
            printf("\nYou're going the HARD way, I see ...");
            MAX = 1000;
            break;
        case 5:
            printf("\n\n/!\\ H A R D C O R E  M O D E  E N G A G E D /!\\\n\n");
            MAX = 5000;
            HARD = 1;
            break;
        default:
            return(0);
            break;
    }
    return(0);
}

int init(int MystNbr)
{
    //Welcoming the user
    printf("\n* * * Welcome to the mystery number game * * *\n");
    
    int manual = diff();
    if(manual) //asking the user to choose difficulty level
    {
        printf("\nChoose the range of the mystery number");
        printf("\nChoose the Minimum : ");
        scanf("  %d%*c",&MIN);

        //Prevent the user from setting a maximum value that is smaller than the minimum value
        MAX = MIN;
        int c = 0;
        while(MAX <=  MIN)
        {   //show error msg to the goddamn stupid user
            if(MAX <= MIN && c > 0)
                printf("\n/!\\ ERROR: The maximum can't be smaller or equal than the minimum /!\\\n");
        
            printf("\nChoose the Maximum : ");
            scanf("  %d%*c",&MAX);
            c++;
        }
    }

    //I just like to verbose a lot
    printf("\nChoosing a random number ... "); 
    while (MystNbr == 0) //generate again if it is 0
    {
        MystNbr = (rand() % (MAX - MIN + 1)) + MIN;
    }
    printf("Ok\n");
    printf("\nNow try to guess my number ");
    printf("it is between %d & %d'\n", MIN, MAX);
    
    //printf("\nMysteryNumber : %d", MystNbr);

    return(MystNbr);
}

int guess(int MystNbr, int inputNbr) //this is where magic happens
{
    int Count = 0;
    while (inputNbr != MystNbr)
    {
        printf("\nEnter your guess : ");
        Count++;
        scanf("  %d%*c", &inputNbr);

        if(inputNbr < MystNbr)
        {
            printf("\nNope the mystery number is GREATER than %d", inputNbr);
        }
        else if(inputNbr > MystNbr)
        {
            printf("\nNope the mystery number is SMALLER than %d", inputNbr);
        }
        else if(inputNbr == MystNbr)
        {
            printf("\nCongratulations YOU little human, found my number,");
            if(Count == 1)
            {
                printf("\nwhich was %d & it took you %d try", MystNbr, Count);
            }else
            {
                printf("\nwhich was %d & it took you %d tries", MystNbr, Count);
            }
            return(1);
        }
        if(HARD && Count >= 15)
        {
            printf("\n\n @ @ @  :O  G A M E  O V E R  O:  @ @ @ \n\n");
            return(2);
        }

        if(HARD)
        {
            printf("\n[%d/15] Try again ...", 15 - Count);
        }
        else
        {
            printf("\n[%d] Try again ...", Count);
        }
    }
    return(0);
}

int replay()
{
    char replay = 0;
    printf("\nDo you wanna replay the game ? \ny/n : ");
    scanf(" %c%*c", &replay);
    
    if(replay == 'y' || replay == 'Y')
    {
        return(1);
    }
    else if(replay == 'n' || replay == 'N')
    {
        return(0);
    }
    return(0);
}

int main()
{
    int MystNbr = 0;
    int rplay = 1;
    srand(time(NULL));  //init rand engine

    while(rplay)
    {
        MystNbr = init(0);  //init Mystery number & print initial dialog
        
        while(!guess(MystNbr, 0));
        
        rplay = replay();
    }
    
    return(0);
}
